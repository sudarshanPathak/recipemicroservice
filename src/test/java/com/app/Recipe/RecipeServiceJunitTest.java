package com.app.Recipe;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.Recipe.entity.Recipe;
import com.app.Recipe.service.RecipeService;



@RunWith(SpringRunner.class)
@SpringBootTest
public class RecipeServiceJunitTest {
	Integer recipeId = 1;
	

	@Autowired
	RecipeService recipeService;
	@Test
	public void getAllRecipeTest() {
		assertFalse(recipeService.getRecipeList().isEmpty());
	}
	@Test
	public void getRecipeDetailsTest() {
		assertNotNull(recipeService.getRecipeDetails(recipeId));

	}



}
