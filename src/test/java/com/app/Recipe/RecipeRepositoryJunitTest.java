package com.app.Recipe;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.Recipe.dao.RecipeRepository;
import com.app.Recipe.entity.Recipe;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RecipeRepositoryJunitTest {

	Integer recipeId = 1 ;
	@Autowired RecipeRepository repository; 

	@Test
	public void getAllRecipeTest() {
		List<Recipe> lstRecipes = repository.findAll();
		assertFalse(lstRecipes.isEmpty());
	}

	@Test
	public void getRecipeDetailsTest() {
		assertNotNull(repository.findByRecipeId(recipeId));
	}
}
