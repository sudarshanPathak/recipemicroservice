package com.app.Recipe;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RecipeControllerJunitTest {
	
	@Autowired
	MockMvc mvc ;
	Integer recipeId = 1;
	String user = "user";
	String password ="password";
	
	@Test
	public void loginUser() throws Exception {
		// @formatter:off
		this.mvc.perform(formLogin().user("user").password("password"))
				.andExpect(authenticated());
		// @formatter:on
	}
	
	@Test
	public void getAllRecipeTest() throws Exception {	
		MvcResult mvcResult = this.mvc.perform(formLogin().user("user").password("password"))
				.andExpect(authenticated()).andReturn();
	

		MockHttpSession httpSession = (MockHttpSession) mvcResult.getRequest().getSession(false);
		this.mvc.perform(get("/recipe/all").session(httpSession)).andExpect(status().isOk());
		
	}
	
	@Test
	public void getRecipeTest() throws Exception {
		MvcResult mvcResult = this.mvc.perform(formLogin().user("user").password("password"))
				.andExpect(authenticated()).andReturn();
	

		MockHttpSession httpSession = (MockHttpSession) mvcResult.getRequest().getSession(false);
		this.mvc.perform(get("/recipe/{recipeId}",recipeId).session(httpSession)).andExpect(status().isOk());
	}

}
