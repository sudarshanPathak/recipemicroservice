package com.app.Recipe;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan(basePackages ="com.app.Recipe")
@EnableSwagger2
public class RecipeApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(RecipeApplication.class, args);
	}
	
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
			.select().apis(RequestHandlerSelectors.any())
			.paths(PathSelectors.any()).build().apiInfo(apiInfo());
		}
	
	private ApiInfo apiInfo() {
	    return new ApiInfo(
	      "Recipe APi", 
	      "API to Fetch ,Add ,Remove and Update Recipes", 
	      "version 1.0", 
	      "Terms of service", 
	      new Contact("sudarshan pathak", "gitlab.com/sudarshanPathak/", "sudarshanPathak@-.com"), 
	      "License of API", "API license URL", Collections.emptyList());
	}
	
}
