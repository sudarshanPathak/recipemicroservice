package com.app.Recipe.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.Recipe.entity.Recipe;
import com.app.Recipe.service.RecipeService;

@RestController
public class RecipeController {
	
	@Autowired  RecipeService recipeService;
	Integer recipeId = 1;
	
	@GetMapping(value = {"/recipe/all","/"})
	public List<Recipe> getAllRecipe(){
		return recipeService.getRecipeList();
	}
	@GetMapping(value="/recipe/{recipeId}")
	public Recipe getRecipeDetails(@PathVariable Integer recipeId) {
		return recipeService.getRecipeDetails(recipeId);
	}
	@PostMapping(value="/recipe/add")
	public void addRecipe(@RequestBody Recipe recipe){
		recipeService.addRecipe(recipe);
	}
	
	@PostMapping(value="/recipe/{recipeId}/update")
	public void updateRecipeDetails(@PathVariable Integer recipeId , @RequestBody Recipe recipe) {
		recipeService.updateRecipe(recipeId ,recipe);
	}
	
	@GetMapping(value = "/recipe/{recipeId}/remove")
	public void removeRecipe(@PathVariable Integer recipeId) {
		recipeService.deleteRecipe(recipeId);
	}
}
