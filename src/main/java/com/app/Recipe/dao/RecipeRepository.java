package com.app.Recipe.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.app.Recipe.entity.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Integer> {
	
	public Recipe findByRecipeId(Integer recipeId);
	
	public List<Recipe> findAll();
	

}
