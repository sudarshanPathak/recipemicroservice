package com.app.Recipe.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.Recipe.dao.RecipeRepository;
import com.app.Recipe.entity.Recipe;

@Service
public class RecipeService {
	@Autowired RecipeRepository repository ;
	
	public List<Recipe> getRecipeList(){
		Recipe recipe = new Recipe(1, "Fruit Salad", new ArrayList<String>(), "Cut and mix", 1, 5, new Date(),true ,false);
		repository.save(recipe);
		return  repository.findAll();	
	}
	
	public Recipe getRecipeDetails(Integer recipeId) {
		return repository.findByRecipeId(recipeId);
	}
	
	public void deleteRecipe(Integer recipeId) {
		repository.deleteById(recipeId);
	}
	public void addRecipe(Recipe recipe) {
		repository.save(recipe);
	}
	public void updateRecipe(Integer recipeId ,Recipe recipe) {
		addRecipe(recipe);
		
		
	}

}
