package com.app.Recipe.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="RECIPE")
public class Recipe implements Serializable{

	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "RECIPE_ID")
	private Integer recipeId;
	@Column(name = "NAME")
	private String name;
	@Column(name = "INGREDIENT")
	@ElementCollection
	private List<String> ingredient;
	@Column(name = "INSTRUCTION")
	private String instruction;
	@Column(name = "FEEDS")
	private Integer feeds;
	@Column(name = "COOK_TIME")
	private Integer cookTime;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON")
	private Date createdOn;
	@Column(name = "IS_VEG")
	private Boolean isVeg;
	@Column(name = "IS_NON_VEG")
	private Boolean isNonVeg;
	public Recipe() {
		
	}
	
	public Recipe(Integer recipeId, String name, List<String> ingredient, String instruction, Integer feeds,
			Integer cookTime, Date createdOn, Boolean isVeg, Boolean isNonVeg) {
		super();
		this.recipeId = recipeId;
		this.name = name;
		this.ingredient = ingredient;
		this.instruction = instruction;
		this.feeds = feeds;
		this.cookTime = cookTime;
		this.createdOn = createdOn;
		this.isVeg = isVeg;
		this.isNonVeg = isNonVeg;
	}

	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(Integer recipeId) {
		this.recipeId = recipeId;
	}
	
	public List<String> getIngredient() {
		return ingredient;
	}

	public void setIngredient(List<String> ingredient) {
		this.ingredient = ingredient;
	}

	public Boolean getIsVeg() {
		return isVeg;
	}

	public void setIsVeg(Boolean isVeg) {
		this.isVeg = isVeg;
	}

	public Boolean getIsNonVeg() {
		return isNonVeg;
	}

	public void setIsNonVeg(Boolean isNonVeg) {
		this.isNonVeg = isNonVeg;
	}

	public String getInstruction() {
		return instruction;
	}
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	public Integer getFeeds() {
		return feeds;
	}
	public void setFeeds(Integer feeds) {
		this.feeds = feeds;
	}
	public Integer getCookTime() {
		return cookTime;
	}
	public void setCookTime(Integer cookTime) {
		this.cookTime = cookTime;
	}
	

}
